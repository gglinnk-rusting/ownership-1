#![allow(unused)]

fn main() {
    {
        let mut string = String::from("Hello");
        string.push_str(", world!");
        println!("{}", string);
    }
    let s1 = String::from("Cacahuètes");
    let mut s2 = s1.clone();
    s2.push_str(" rouges !");

    println!("{}\n{}", s2, s1);

    let mut get_str = gives_ownership();
    get_str.push_str(" Goodbye day !");
    println!("{}", get_str);
}

fn gives_ownership() -> String {
    let string = String::from("Hello World !");
    string
}
